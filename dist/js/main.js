$(document).ready(function () {
  // Анимация
  new WOW().init();

  // Маска
  $("input[type=tel]").mask("+7 (999) 999-9999");

  // Действие ссылок
  $(function () {
    $("a").on("click", function (event) {
      if ($(this).hasClass("no-event")) return;
      event.preventDefault();
      var sc = $(this).attr("href"),
        dn = $(sc).offset().top;
      $("html, body").animate({ scrollTop: dn }, 1000);
    });
  });

  // Действие кнопок
  $(".button")
    .mousedown(function () {
      $(this).addClass("button_scale ");
    })
    .click(function () {
      return false;
    })
    .mouseup(function () {
      $(this).removeClass("button_scale");
    });

  // Действие кнопок
  $(".button").click(function (e) {
    e.preventDefault();
    $(this).addClass("button_scale");
    setTimeout(function () {
      $(".button").removeClass("button_scale");
    }, 300);
  });

  // Картинка
  $(".image").click(function () {
    // Событие клика на маленькое изображение
    var img = $(this); // Получаем изображение, на которое кликнули
    var src = img.attr("src");
    console.log(src); // Достаем из этого изображения путь до картинки
    $("body").append(
      "<div class='popup'>" + //Добавляем в тело документа разметку всплывающего окна
        "<div class='popup_bg'></div>" +
        `<div class='popup-block'>
      <img src='${src}' class='popup_img' />
      <a class='popup-close' href='#'>x</a>
      </div>` +
        "</div>"
    );
    $(".popup").fadeIn(600); // Медленно выводим изображение
    $(".popup_bg").click(function () {
      $(".popup").fadeOut(600);
      setTimeout(function () {
        $(".popup").remove();
      }, 600);
    });
    $(".popup-close").click(function (e) {
      e.preventDefault();
      $(".popup").fadeOut(600);
      setTimeout(function () {
        $(".popup").remove();
      }, 600);
    });
  });

  $("#call_1").on("click", () => {
    setTimeout(function () {
      if (
        $(".stocks-form__phone").val().trim() == "" ||
        $(".stocks-form__name").val().trim() == ""
      ) {
        alert(
          "Не удалось оставить заявку. Пожалуйста, проверьте правильность введенных данных!"
        );
        return;
      }

      $.post(
        "/backend.php",
        {
          type: "call_1",
          phone: $(".stocks-form__phone").val(),
          name: $(".stocks-form__name").val(),
        },
        (data, ts, aa) => {
          if (JSON.parse(data).success) {
            alert("Ваша заявка принята, скоро мы с Вами свяжемся!");
          } else {
            alert(
              "Не удалось оставить заявку. Пожалуйста, повторите попытку позднее!"
            );
          }
        }
      );
    }, 300);
  });

  $("#call_0").on("click", () => {
    if ($(".modal-bg__phone").val().trim() == "") {
      alert(
        "Не удалось оставить заявку. Пожалуйста, проверьте правильность введенных данных!"
      );
      return;
    }

    if ($("#call_0_box").val() != "on") {
      alert(
        "Не удалось оставить заявку. Примите согласие на обработку персональных данных для продолжения."
      );
      return;
    }

    $.post(
      "/backend.php",
      {
        type: "call_0",
        phone: $(".modal-bg__phone").val(),
      },
      (data, ts, aa) => {
        if (JSON.parse(data).success) {
          alert("Ваша заявка принята, скоро мы с Вами свяжемся!");
        } else {
          alert(
            "Не удалось оставить заявку. Пожалуйста, повторите попытку позднее!"
          );
        }
      }
    );
  });

  $("#call_3").on("click", () => {
    if ($("#phone_3").val().trim() == "") {
      alert(
        "Не удалось оставить заявку. Пожалуйста, проверьте правильность введенных данных!"
      );
      return;
    }

    $.post(
      "/backend.php",
      {
        type: "call_0",
        phone: $("#phone_3").val(),
      },
      (data, ts, aa) => {
        if (JSON.parse(data).success) {
          alert("Ваша заявка принята, скоро мы с Вами свяжемся!");
        } else {
          alert(
            "Не удалось оставить заявку. Пожалуйста, повторите попытку позднее!"
          );
        }
      }
    );
  });

  $("#call_2").on("click", () => {
    if (
      $(".call-form__phone").val().trim() == "" ||
      $(".call-form__name").val().trim() == "" ||
      $(".call-form__address").val().trim() == ""
    ) {
      alert(
        "Не удалось оставить заявку. Пожалуйста, проверьте правильность введенных данных!"
      );
      return;
    }

    $.post(
      "/backend.php",
      {
        type: "measurer",
        phone: $(".call-form__phone").val(),
        name: $(".call-form__name").val(),
        address: $(".call-form__address").val(),
        date: $(".call-form__date").val(),
        time: $(".call-form__time").val(),
        comment: $(".call-form__comment").val(),
      },
      (data, ts, aa) => {
        if (JSON.parse(data).success) {
          alert("Ваша заявка принята, скоро мы с Вами свяжемся!");
        } else {
          alert(
            "Не удалось оставить заявку. Пожалуйста, повторите попытку позднее!"
          );
        }
      }
    );
  });

  // parallax
  $(function () {
    window.addEventListener("scroll", function (event) {
      var top = this.pageYOffset;
      var layers = $(".parallax");
      var speed, yPos;
      layers.each(function () {
        speed = $(this).attr("data-speed");
        var yPos = -((top * speed) / 100);
        $(this).attr("style", "transform: translateY(" + yPos + "px)");
      });
    });
  });
});

// пункты меню
$(window).on("scroll", function () {
  if ($(window).scrollTop() <= $(".stocks").height()) {
    $(".nav li a svg").removeClass("active");
    $(".nav li a .icon1").addClass("active");
  }
  for (let i = 0; i <= 7; i++) {
    if ($(window).scrollTop() >= ($(".stocks").height() - 5) * i) {
      $(".nav li a svg").removeClass("active");
      $(`.nav li a .icon${i + 1}`).addClass("active");
    }
  }
});

// Закрыть меню при нажатии на пункт
$(document).ready(function () {
  $(".nav-link").click(function () {
    $(".nav__btn").click();
  });
});

// Модалка
$(document).ready(function () {
  $("#modal-button").click(function (e) {
    e.preventDefault();
    $(".modal").fadeIn(600);
  });
  $("#modal-button_inside").click(function (e) {
    e.preventDefault();
    $(".nav__btn").click();
    $(".modal").fadeIn(600);
  });
  $(".modal-bg__close").click(function (e) {
    e.preventDefault();
    $(".modal").fadeOut(600);
  });
  $(".modal-back").click(function () {
    $(".modal").fadeOut(600);
  });
});

// + в - (faq)
$(document).ready(function () {
  let $btn = $(".faq-accordion__btn");
  $btn.click(function () {
    let $this = $(this).find("span span");
    $this.toggleClass("active");
    console.log($(this).siblings());
    $(this)
      .parent()
      .siblings()
      .find(".faq-accordion__btn span span")
      .removeClass("active");
  });
});

// Калькулятор
function recalculate_price() {
  var square_price = 350;
  var trub_price = 100;
  var svet_price = 250;
  var ugol_price = 100;
  var kar_price = 300;

  var square;
  var trub;
  var svet;
  var ugol;
  var kar;

  square = $("#calc-output").html();
  trub = $("#calc-input_tube").val();
  svet = $("#calc-input_lamp").val();
  ugol = $("#calc-input_square").val();
  kar = $("#calc-input_cornice").val();

  var per = 0;
  if (square >= 30 && square <= 50) {
    per = 10;
  } else if (square >= 51) {
    per = 15;
  }

  var price =
    square_price * square +
    trub_price * trub +
    svet * svet_price +
    kar_price * kar;

  if (ugol > 4) {
    price = price + ugol_price * Math.abs(4 - ugol);
  }

  $("#calc-output_discount").html(per);
  $("#calc-output_price").html(price);
}

function recalculate_sqr() {
  $("#calc-output").html(
    $("#calc-input_slider_height").val() * $("#calc-input_slider_width").val()
  );

  recalculate_price();
}

function update_height(h, animated) {
  h = h < 32 ? 32 : h;

  if (!animated) {
    $(".calc-block__area-block__minibox").css("height", h);
  } else {
    $(".calc-block__area-block__minibox").animate(
      { height: h },
      { duration: 50 }
    );
  }

  recalculate_sqr();
}

function update_width(w, animated) {
  w = w < 32 ? 32 : w;

  if (!animated) {
    $(".calc-block__area-block__minibox").css("width", w);
  } else {
    $(".calc-block__area-block__minibox").animate(
      { width: w },
      { duration: 50 }
    );
  }

  recalculate_sqr();
}

function clamp(e, min, max) {
  if (e.val() > max) {
    e.val(max);
  }

  if (e.val() < min) {
    e.val(min);
  }
}

$(document).on("input", "#calc-input_slider_height", function () {
  $("#calc-input_text_height").val($(this).val());
  update_height(
    ($(this).val() / 50) * $(".calc-block__area-block__box").height()
  );
});

$(document).on("input", "#calc-input_slider_width", function () {
  $("#calc-input_text_width").val($(this).val());
  update_width(
    ($(this).val() / 50) * $(".calc-block__area-block__box").width()
  );
});

$(document).on("input", "#calc-input_text_width", function () {
  clamp($(this), 1, 50);

  $("#calc-input_slider_width").val($(this).val());
  update_width(
    ($(this).val() / 50) * $(".calc-block__area-block__box").width(),
    true
  );
});

$(document).on("input", "#calc-input_text_height", function () {
  clamp($(this), 1, 50);

  $("#calc-input_slider_width").val($(this).val());
  update_height(
    ($(this).val() / 50) * $(".calc-block__area-block__box").height(),
    true
  );
});

$(document).on("input", "#calc-input_tube", function () {
  recalculate_price();
});

$(document).on("input", "#calc-input_lamp", function () {
  recalculate_price();
});

$(document).on("input", "#calc-input_cornice", function () {
  recalculate_price();
});

$(document).on("input", "#calc-input_square", function () {
  recalculate_price();
});

// Подключение слайдеров для секции call
$(document).ready(function () {
  $(".portfolio-block__nav").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    focusOnSelect: true,
    draggable: true,
    variableWidth: true,
    prevArrow: $(),
    nextArrow: $(".portfolio-block__nav-arrow"),
    asNavFor: ".portfolio-block__content",
  });
  $(".portfolio-block__content").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    adaptiveHeight: true,
    draggable: false,
    swipeToSlide: false,
    swipe: false,
    fade: true,
    asNavFor: ".portfolio-block__nav",
  });
  $(".portfolio-block__content-img").each(function () {
    $(this).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: true,
      prevArrow: $(),
      nextArrow: $(".portfolio-block__content-arrow", $(this).parent()),
    });
  });
});
